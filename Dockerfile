FROM maven:3.9.0-eclipse-temurin-17-alpine

# https://pkgs.alpinelinux.org/packages?name=nodejs&branch=v3.17
# https://pkgs.alpinelinux.org/packages?name=npm&branch=v3.17
RUN apk add --update nodejs=18.16.0-r0 npm=9.1.2-r0
